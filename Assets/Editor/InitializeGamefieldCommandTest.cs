using NUnit.Framework;
using strange.extensions.injector.api;
using strange.extensions.command.api;
using strange.extensions.injector.impl;
using strange.framework.api;
using strange.extensions.command.impl;
using tictactoe;
using tictactoe.game;
using System.Linq;

[TestFixture]
public class InitializeGamefieldCommandTest {

  IInjectionBinder injectionBinder;
  ICommandBinder commandBinder;

  [SetUp]
  public void SetUp()
  {
    injectionBinder = new InjectionBinder();
    injectionBinder.Bind<IInjectionBinder>()
      .Bind<IInstanceProvider>().ToValue(injectionBinder);
    injectionBinder.Bind<ICommandBinder>()
      .To<SignalCommandBinder>().ToSingleton();
    commandBinder = injectionBinder.GetInstance<ICommandBinder>();

    injectionBinder.Bind<GameModel>().ToSingleton();

    injectionBinder.Bind<StartSignal>().ToSingleton();
    injectionBinder.Bind<GamefieldInitializedSignal>().ToSingleton();

    commandBinder.Bind<StartSignal>().To<InitializeGamefieldCommand>();
  }

  [Test]
  public void InitializeGamefield()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();

    GamefieldInitializedSignal gamefieldInitializedSignal = injectionBinder
      .GetInstance<GamefieldInitializedSignal>();

    StartSignal startSignal = injectionBinder
      .GetInstance<StartSignal>();

    int tileCount = 9;

    bool gamefieldInitialized = false;
    gamefieldInitializedSignal.AddOnce(
      ids =>
      {
        gamefieldInitialized = true;
        Assert.True(ids.SequenceEqual(Enumerable.Range(0, tileCount)));
      }
    );

    startSignal.Dispatch();

    bool tilesInitialized = true;

    for (int i = 0; i < tileCount; i++)
    {
      if (gameModel.tiles[i] == null ||
          gameModel.tiles[i].id != i ||
          gameModel.tiles[i].state != TileState.CLEAN)
      {
        tilesInitialized = false;
      }
    }

    Assert.True(gamefieldInitialized);

    Assert.True(tilesInitialized);
    Assert.True(gameModel.gameInProgress);
  }
}
