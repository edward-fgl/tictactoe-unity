using NUnit.Framework;
using System.Collections.Generic;
using strange.extensions.injector.api;
using strange.extensions.command.api;
using strange.extensions.injector.impl;
using strange.framework.api;
using strange.extensions.command.impl;
using tictactoe.game;

[TestFixture]
public class MarkTileCommandTest {

  IInjectionBinder injectionBinder;
  ICommandBinder commandBinder;

  [SetUp]
  public void SetUp()
  {
    injectionBinder = new InjectionBinder();
    injectionBinder.Bind<IInjectionBinder>()
      .Bind<IInstanceProvider>().ToValue(injectionBinder);
    injectionBinder.Bind<ICommandBinder>()
      .To<SignalCommandBinder>().ToSingleton();
    commandBinder = injectionBinder.GetInstance<ICommandBinder>();

    injectionBinder.Bind<GameModel>().ToSingleton();

    injectionBinder.Bind<UpdateTileSignal>().ToSingleton();

    commandBinder.Bind<MarkTileSignal>().To<MarkTileCommand>();
  }

  [Test]
  public void MarkTile()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();

    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();

    UpdateTileSignal updateTileSignal = injectionBinder
      .GetInstance<UpdateTileSignal>();

    // initialize gameModel
    gameModel.gameInProgress = true;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }

    const int tileId = 0;

    bool tileUpdated = false;
    updateTileSignal.AddOnce(
      (id, tileState) =>
      {
        tileUpdated = true;
        Assert.True(id == tileId);
        Assert.True(tileState == TileState.X);
      }
    );

    markTileSignal.Dispatch(tileId);

    Assert.True(tileUpdated);

    Assert.True(gameModel.currentPlayer == CurrentPlayer.PLAYER_TWO);
    Assert.True(gameModel.tiles[tileId].state == TileState.X);
  }

  [Test]
  public void MarkTile_InvalidID()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    UpdateTileSignal updateTileSignal = injectionBinder
      .GetInstance<UpdateTileSignal>();

    int[] tileIds = new [] {-1, int.MaxValue};

    gameModel.gameInProgress = true;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }

    bool tilesWereNotUpdated = true;

    foreach (int tileId in tileIds)
    {
      updateTileSignal.AddOnce((id, state) => { tilesWereNotUpdated = false; });
      markTileSignal.Dispatch(tileId);
    }

    Assert.True(tilesWereNotUpdated);

    Assert.True(gameModel.currentPlayer == CurrentPlayer.PLAYER_ONE);
  }

  [Test]
  public void MarkTile_Marked()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    UpdateTileSignal updateTileSignal = injectionBinder
      .GetInstance<UpdateTileSignal>();

    int tileId = 0;

    gameModel.gameInProgress = true;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      var tileState = i == tileId ? TileState.X : TileState.CLEAN;
      gameModel.tiles.Add(i, new Tile(i, tileState));
    }

    bool tileWasNotUpdated = true;
    updateTileSignal.AddOnce((id, state) => { tileWasNotUpdated = false; });

    markTileSignal.Dispatch(tileId);

    Assert.True(tileWasNotUpdated);

    Assert.True(gameModel.currentPlayer == CurrentPlayer.PLAYER_ONE);
    Assert.True(gameModel.tiles[tileId].state == TileState.X);
  }
}