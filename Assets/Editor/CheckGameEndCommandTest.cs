using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.injector.api;
using strange.extensions.command.api;
using strange.extensions.injector.impl;
using strange.framework.api;
using strange.extensions.command.impl;
using tictactoe.game;
using tictactoe;
using UnityEngine;

public class GamefieldComboPair {
  public int[] tiles { get; set; }
  public int[] winningTiles { get; set; }

  public GamefieldComboPair(int[] tiles, int[] winningTiles) {
    this.tiles = tiles;
    this.winningTiles = winningTiles;
  }

} 

[TestFixture]
public class CheckGameEndCommandTest {

  IInjectionBinder injectionBinder;
  ICommandBinder commandBinder;

  [SetUp]
  public void SetUp()
  {
    injectionBinder = new InjectionBinder();
    injectionBinder.Bind<IInjectionBinder>()
      .Bind<IInstanceProvider>().ToValue(injectionBinder);
    injectionBinder.Bind<ICommandBinder>()
      .To<SignalCommandBinder>().ToSingleton();
    commandBinder = injectionBinder.GetInstance<ICommandBinder>();

    injectionBinder.Bind<GameModel>().ToSingleton();

    injectionBinder.Bind<MarkTileSignal>().ToSingleton();
    injectionBinder.Bind<GameEndSignal>().ToSingleton();
    injectionBinder.Bind<StartSignal>().ToSingleton();

    commandBinder.Bind<MarkTileSignal>().To<CheckGameEndCommand>();
    commandBinder.Bind<StartSignal>().To<CheckGameEndCommand>();
  }

  [Test]
  public void CheckGameEnd_Row()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    // initialize tiles
    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }

    var gamefields = new GamefieldComboPair[]
    {
      new GamefieldComboPair(
        new [] {
          1, 1, 1,
          1, 1, 1,
          1, 1, 1
        },
        new [] { 0, 1, 2 }
      ),
      new GamefieldComboPair(
        new [] {
          0, 1, 1,
          1, 1, 1,
          1, 1, 1
        },
        new [] { 3, 4, 5 }
      ),
      new GamefieldComboPair(
        new [] {
          0, 1, 1,
          0, 1, 0,
          1, 1, 1
        },
        new [] { 6, 7, 8 }
      ),
    };

    bool gameEnded = false;

    foreach (GamefieldComboPair pair in gamefields)
    {
      for (int i = 0; i < 9; i++)
      {
        var tileState = pair.tiles[i] == 1 ? TileState.X : TileState.O;
        gameModel.tiles[i].state = tileState;
      }
      
      gameEndSignal.AddOnce(
        winningTileIds =>
        {
          gameEnded = true;
          Assert.True(winningTileIds.SequenceEqual(pair.winningTiles));
        }
      );
      markTileSignal.Dispatch(0);
    }

    Assert.True(gameEnded);
    Assert.True(!gameModel.gameInProgress);
  }

  [Test]
  public void CheckGameEnd_Column()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }

    var gamefields = new GamefieldComboPair[]
    {
      new GamefieldComboPair(
        new [] {
          1, 0, 1,
          1, 0, 1,
          1, 0, 1
        },
        new [] { 0, 3, 6 }
      ),
      new GamefieldComboPair(
        new [] {
          1, 1, 0,
          0, 1, 1,
          0, 1, 1
        },
        new [] { 1, 4, 7 }
      ),
      new GamefieldComboPair(
        new [] {
          1, 0, 1,
          0, 1, 1,
          0, 1, 1
        },
        new [] { 2, 5, 8 }
      ),
    };

    bool gameEnded = false;

    foreach (GamefieldComboPair pair in gamefields)
    {
      for (int i = 0; i < 9; i++)
      {
        var tileState = pair.tiles[i] == 1 ? TileState.X : TileState.O;
        gameModel.tiles[i].state = tileState;
      }
      
      gameEndSignal.AddOnce(
        winningTileIds =>
        {
          gameEnded = true;
          Assert.True(winningTileIds.SequenceEqual(pair.winningTiles));
        }
      );
      markTileSignal.Dispatch(0);
    }

    Assert.True(gameEnded);
    Assert.True(!gameModel.gameInProgress);
  }

  [Test]
  public void CheckGameEnd_Diagonal()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }

    var gamefields = new GamefieldComboPair[]
    {
      new GamefieldComboPair(
        new [] {
          1, 0, 1,
          0, 1, 0,
          1, 0, 1
        },
        new [] { 0, 4, 8 }
      ),
      new GamefieldComboPair(
        new [] {
          0, 0, 1,
          0, 1, 0,
          1, 0, 1
        },
        new [] { 2, 4, 6 }
      ),
    };

    bool gameEnded = false;

    foreach (GamefieldComboPair pair in gamefields)
    {
      for (int i = 0; i < 9; i++)
      {
        var tileState = pair.tiles[i] == 1 ? TileState.X : TileState.O;
        gameModel.tiles[i].state = tileState;
      }
      
      gameEndSignal.AddOnce(
        winningTileIds =>
        {
          gameEnded = true;
          Assert.True(winningTileIds.SequenceEqual(pair.winningTiles));
        }
      );
      markTileSignal.Dispatch(0);
    }

    Assert.True(gameEnded);
    Assert.True(!gameModel.gameInProgress);
  }


  [Test]
  public void CheckGameEnd_Draw()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    var tiles = new []
    {
      0, 1, 1,
      1, 0, 0,
      0, 1, 1
    };

    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      var tileState = tiles[i] == 1 ? TileState.X : TileState.O;
      gameModel.tiles.Add(i, new Tile(i, tileState));
    }
 
    bool gameEnded = false;
    gameEndSignal.AddOnce(
      winningTileIds =>
      {
        gameEnded = true;
        Assert.True(winningTileIds.Length == 0);
      }
    );

    markTileSignal.Dispatch(0);

    Assert.True(gameEnded);
    Assert.True(!gameModel.gameInProgress);
  }

  [Test]
  public void CheckGameEnd_EmptyGamefield() {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    StartSignal startSignal = injectionBinder
      .GetInstance<StartSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    gameModel.gameInProgress = true;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
    }
 
    bool gameContinues = true;
    gameEndSignal.AddOnce(ids => { gameContinues = false; });

    startSignal.Dispatch();

    Assert.True(gameContinues);
    Assert.True(gameModel.gameInProgress);
  }

  [Test]
  public void CheckGameEnd_MultipleWinCombos() {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();
    MarkTileSignal markTileSignal = injectionBinder
      .GetInstance<MarkTileSignal>();
    GameEndSignal gameEndSignal = injectionBinder
      .GetInstance<GameEndSignal>();

    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.X));
    }
 
    bool gameEnded = false;
    gameEndSignal.AddOnce(
      winningTileIds =>
      {
        gameEnded = true;
        Assert.True(winningTileIds.SequenceEqual(new [] {0, 1, 2}));
      }
    );

    markTileSignal.Dispatch(0);

    Assert.True(gameEnded);
    Assert.True(!gameModel.gameInProgress);
  }
}