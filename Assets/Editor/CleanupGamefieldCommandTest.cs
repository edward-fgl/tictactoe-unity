using NUnit.Framework;
using System.Collections.Generic;
using strange.extensions.injector.api;
using strange.extensions.command.api;
using strange.extensions.injector.impl;
using strange.framework.api;
using strange.extensions.command.impl;
using tictactoe.game;
using tictactoe;

[TestFixture]
public class CleanupGamefieldCommandTest {

  IInjectionBinder injectionBinder;
  ICommandBinder commandBinder;

  [SetUp]
  public void SetUp()
  {
    injectionBinder = new InjectionBinder();
    injectionBinder.Bind<IInjectionBinder>()
      .Bind<IInstanceProvider>().ToValue(injectionBinder);
    injectionBinder.Bind<ICommandBinder>()
      .To<SignalCommandBinder>().ToSingleton();
    commandBinder = injectionBinder.GetInstance<ICommandBinder>();

    injectionBinder.Bind<GameModel>().ToSingleton();

    injectionBinder.Bind<GameRestartSignal>().ToSingleton();

    commandBinder.Bind<GameRestartSignal>().To<CleanupGamefieldCommand>();
  }

  [Test]
  public void CleanupGamefield()
  {
    GameModel gameModel = injectionBinder.GetInstance<GameModel>();

    GameRestartSignal gameRestartSignal = injectionBinder
      .GetInstance<GameRestartSignal>();

    // initialize gameModel
    gameModel.gameInProgress = false;
    gameModel.tiles = new Dictionary<int, Tile>();
    for (int i = 0; i < 9; i++)
    {
      gameModel.tiles.Add(i, new Tile(i, TileState.X));
    }

    gameRestartSignal.Dispatch();

    bool tilesAreClean = true;
    for (int i = 0; i < 9; i++)
    {
      if (gameModel.tiles[i].state != TileState.CLEAN)
      {
        tilesAreClean = false;
      }
    }

    Assert.True(tilesAreClean);
    Assert.True(gameModel.currentPlayer == CurrentPlayer.PLAYER_ONE);
    Assert.True(gameModel.gameInProgress);
  }
}