using UnityEngine;
using System;
using strange.extensions.mediation.impl;

namespace tictactoe.game {
  public class GameOverMediator : Mediator {
    [Inject]
    public GameEndSignal gameEndSignal { get; set; }

    [Inject]
    public GameRestartSignal gameRestartSignal { get; set; }

    [Inject]
    public GameOverView view { get; set; }

    public override void OnRegister() {
      gameEndSignal.AddListener(show);
      gameRestartSignal.AddListener(hide);
      view.RestartButtonClicked += restart;
    }

    private void restart() {
      gameRestartSignal.Dispatch();
    }

    public override void OnRemove() {
      gameEndSignal.RemoveListener(show);
      gameRestartSignal.RemoveListener(hide);      
      view.RestartButtonClicked -= restart;
    }

    private void show(int[] winningTiles) {
      gameObject.SetActive(true);
    }

    private void hide() {
      gameObject.SetActive(false);
    }
  }
}