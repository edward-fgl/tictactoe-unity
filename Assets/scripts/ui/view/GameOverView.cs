using UnityEngine;
using UnityEngine.UI;
using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace tictactoe.game {

  public class GameOverView : View {

    public Button restartButton;
    public event Action RestartButtonClicked;

    void OnEnable() {
      restartButton.onClick.AddListener(clicked);
    }

    void OnDisable() {
      restartButton.onClick.RemoveListener(clicked);
    }

    void clicked() {
      if (RestartButtonClicked != null) {
        RestartButtonClicked();
      }
    }

  }
}

