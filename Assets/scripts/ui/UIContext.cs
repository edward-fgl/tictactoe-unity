using System;
using UnityEngine;
using strange.extensions.context.impl;
using tictactoe.game;

namespace tictactoe.main
{
  public class UIContext : SignalContext {

    public UIContext(MonoBehaviour contextView) : base(contextView) {}      

    protected override void mapBindings() {
      base.mapBindings();

      commandBinder.Bind<StartSignal>()
        .To<UIStartCommand>().Once();

      mediationBinder.Bind<GameOverView>().To<GameOverMediator>();
    }

    protected override void postBindings() {
      base.postBindings();

      AudioListener listener = (contextView as GameObject).GetComponentInChildren<AudioListener>();
      listener.enabled = false;
    }
  }
}

