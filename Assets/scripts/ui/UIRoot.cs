using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace tictactoe.main
{
  public class UIRoot : ContextView
  {
    void Awake()
    {
      context = new UIContext(this);
    }
  }
}

