using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using UnityEngine.SceneManagement;

namespace tictactoe.main {
  public class MainStartupCommand : Command {
    [Inject(ContextKeys.CONTEXT_VIEW)]
    public GameObject contextView { get; set; }

    override public void Execute() {
      SceneManager.LoadScene("Game", LoadSceneMode.Additive);
      SceneManager.LoadScene("UI", LoadSceneMode.Additive);
    }
  }
}