﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace tictactoe.main
{
	public class MainRoot : ContextView
	{
		void Awake()
		{
			context = new MainContext(this);
		}
	}
}

