﻿using System;
using UnityEngine;
using strange.extensions.context.impl;
using tictactoe.game;

namespace tictactoe.main
{
	public class MainContext : SignalContext {

    public MainContext(MonoBehaviour contextView) : base(contextView) {}		  

    protected override void mapBindings() {
      base.mapBindings();
      commandBinder.Bind<StartSignal>().To<MainStartupCommand>();

      injectionBinder.Bind<GameModel>().ToSingleton().CrossContext();
      injectionBinder.Bind<GameEndSignal>().ToSingleton().CrossContext();
      injectionBinder.Bind<GameRestartSignal>().ToSingleton().CrossContext();
    }
	}
}

