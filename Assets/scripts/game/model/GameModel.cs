using System;
using System.Collections.Generic;

namespace tictactoe.game {

  public enum TileState {
    CLEAN,
    X,
    O,
  }

  public enum CurrentPlayer {
    PLAYER_ONE,
    PLAYER_TWO
  } 

  public class Tile {
    public int id;
    public TileState state;

    public Tile(int id, TileState state) {
      this.id = id;
      this.state = state;
    }
  }

  public class GameModel {
    public Dictionary<int, Tile> tiles { get; set; }
    public CurrentPlayer currentPlayer { get; set; }
    public bool gameInProgress { get; set; }
  }
}
