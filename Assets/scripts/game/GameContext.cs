using UnityEngine;
using strange.extensions.context.impl;

namespace tictactoe.game {
  public class GameContext : SignalContext {
    public GameContext(MonoBehaviour contextView) : base(contextView) {}

    protected override void mapBindings() {
      base.mapBindings();

      commandBinder.Bind<StartSignal>()
        .To<GameStartCommand>().Once()
        .To<InitializeGamefieldCommand>().Once();
      commandBinder.Bind<MarkTileSignal>()
        .To<MarkTileCommand>()
        .To<CheckGameEndCommand>()
        .InSequence();
      commandBinder.Bind<GameEndSignal>()
        .To<GameEndCommand>();
      commandBinder.Bind<GameRestartSignal>()
        .To<CleanupGamefieldCommand>();

      mediationBinder.Bind<TileView>().To<TileMediator>();
      mediationBinder.Bind<GamefieldView>().To<GamefieldMediator>();

      injectionBinder.Bind<UpdateTileSignal>().ToSingleton();
      injectionBinder.Bind<GamefieldInitializedSignal>().ToSingleton();
    }
  }
}

