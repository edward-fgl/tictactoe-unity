using UnityEngine;
using strange.extensions.context.impl;
using System.Collections;

namespace tictactoe.game {
  public class GameRoot : ContextView {
    void Start() {
      context = new GameContext(this);
    }
  }
}