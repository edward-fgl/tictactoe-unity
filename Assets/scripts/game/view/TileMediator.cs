using System;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace tictactoe.game {
  public class TileMediator : Mediator {
    [Inject]
    public TileView view { get; set; }

    [Inject]
    public MarkTileSignal markTileSignal { get; set; }

    [Inject]
    public GameRestartSignal gameRestartSignal { get; set; }

    private void OnMouseDown() {
      markTileSignal.Dispatch(view.id);
    }

    public override void OnRegister() {
      gameRestartSignal.AddListener(resetTiles);
    }

    private void resetTiles() {
      view.resetTile();
    }
  }
}