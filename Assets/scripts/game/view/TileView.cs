using UnityEngine;
using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace tictactoe.game {

  public class TileView : View {

    public int id;

    internal void setSprite(TileState tileState) {
      string spriteName = tileState == TileState.X
                            ? "tile_X"
                            : "tile_O";
      Sprite sprite = Resources.Load<Sprite>(spriteName);
      GetComponent<SpriteRenderer>().sprite = sprite;
    }

    internal void highlight() {
      GetComponent<SpriteRenderer>().color = new Color(127/255f, 1f, 116/255f, 1f);
    }

    internal void resetTile() {
      Sprite sprite = Resources.Load<Sprite>("tile_empty");
      GetComponent<SpriteRenderer>().sprite = sprite;
      GetComponent<SpriteRenderer>().color = new Color(193/255f, 193/255f, 193/255f, 1f);
    }
  }
}
