using UnityEngine;
using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections.Generic;

namespace tictactoe.game {

  public class GamefieldView : View {

    public Dictionary<int, TileView> idTileMap;
  
    internal void createTiles(int[] ids) {
      idTileMap = new Dictionary<int, TileView>();
      int idIndex = 0;

      var rowLength = Math.Sqrt(ids.Length);

      for (int row = 0; row < rowLength; row++) {

        for (int column = 0; column < rowLength; column++) {

          GameObject tile = Resources.Load<GameObject>("Tile");
          
          var tileGO = GameObject.Instantiate<GameObject>(
                        tile, 
                        new Vector3(column - 1, row - 1, 0), 
                        Quaternion.identity);
          
          tileGO.transform.parent = gameObject.transform;

          int id = ids[idIndex++];

          var tileView = tileGO.GetComponent<TileView>();
          tileView.id = id;
          idTileMap.Add(id, tileView);
        }
      }
    }


  }
}