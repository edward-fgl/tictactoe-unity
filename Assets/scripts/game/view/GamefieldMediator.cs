using System;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace tictactoe.game {
  public class GamefieldMediator : Mediator {
    [Inject]
    public GamefieldInitializedSignal gamefieldInitializedSignal { get; set; }

    [Inject]
    public UpdateTileSignal updateTileSignal { get; set; }

    [Inject]
    public GamefieldView view { get; set; }

    [Inject]
    public GameEndSignal gameEndSignal { get; set; }

    public override void OnRegister() {
      gamefieldInitializedSignal.AddListener(onInitialization);
      updateTileSignal.AddListener(onTileUpdate);
      gameEndSignal.AddListener(onTileHighlight);
    }

    private void onInitialization(int[] ids) {
      view.createTiles(ids);
    }

    private void onTileUpdate(int tileId, TileState tileState) {
      view.idTileMap[tileId].setSprite(tileState);
    }

    private void onTileHighlight(int[] tileIds) {
      foreach (int id in tileIds) {
        view.idTileMap[id].highlight();
      }
    }

  }
}