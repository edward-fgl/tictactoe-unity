using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace tictactoe.game {
  public class MarkTileSignal : Signal<int>{}
  public class InitializeGamefieldSignal : Signal{}
  public class GamefieldInitializedSignal : Signal<int[]>{}
  public class UpdateTileSignal : Signal<int, TileState>{}
}