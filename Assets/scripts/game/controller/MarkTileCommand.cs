using System;
using UnityEngine;
using strange.extensions.command.impl;

namespace tictactoe.game {
  public class MarkTileCommand : Command {

    [Inject]
    public int tileId { get; set; }

    [Inject]
    public UpdateTileSignal updateTileSignal { get; set; }

    [Inject]
    public GameModel gameModel { get; set; }

    public override void Execute() {
      var tiles = gameModel.tiles;
 
      if (tileId > -1 && tileId < tiles.Keys.Count &&
          tiles[tileId].state == TileState.CLEAN &&
          gameModel.gameInProgress) 
      {
        TileState tileState =
          gameModel.currentPlayer == CurrentPlayer.PLAYER_ONE
            ? TileState.X
            : TileState.O;

        gameModel.currentPlayer =
          gameModel.currentPlayer == CurrentPlayer.PLAYER_ONE
            ? CurrentPlayer.PLAYER_TWO
            : CurrentPlayer.PLAYER_ONE;

        tiles[tileId].state = tileState;
              
        updateTileSignal.Dispatch(tileId, tileState);
      }
        
    }
  }
}