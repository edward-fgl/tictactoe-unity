using System;
using UnityEngine;
using strange.extensions.command.impl;

namespace tictactoe.game {
  public class CheckGameEndCommand : Command {
    [Inject]
    public GameModel gameModel { get; set; }

    [Inject]
    public GameEndSignal gameEndSignal { get; set; }

    int[] winningTiles = new int[] {};

    bool checkRows()
    {
      var tiles = gameModel.tiles;
      for (int row = 0; row < 9; row += 3) {
        TileState possibleWinTileType = gameModel.tiles[row].state;
        if (possibleWinTileType != TileState.CLEAN) {
          if (gameModel.tiles[row + 1].state == possibleWinTileType &&
              gameModel.tiles[row + 2].state == possibleWinTileType) {
            this.winningTiles = new int[] { row + 0, row + 1, row + 2 };
            return true;
          }
        }
      }
      return false;
    }

    bool checkColumns()
    {
      var tiles = gameModel.tiles;
      for (int column = 0; column < 3; column++) {
        TileState possibleWinTileType = gameModel.tiles[column].state;
        if (possibleWinTileType != TileState.CLEAN) {
          if (gameModel.tiles[column + 3].state == possibleWinTileType &&
              gameModel.tiles[column + 6].state == possibleWinTileType) {
            this.winningTiles = new int[] { column, column + 3, column + 6 };
            return true;
          }
        }
      }
      return false;
    }

    bool checkDiagonals()
    {
      var tiles = gameModel.tiles;
      if (tiles[4].state != TileState.CLEAN) {
        TileState possibleWinTileType = tiles[4].state;

        if (tiles[0].state == possibleWinTileType &&
            tiles[8].state == possibleWinTileType) {
          this.winningTiles = new int[] { 0, 4, 8 };
          return true;
        } else if (tiles[2].state == possibleWinTileType &&
                   tiles[6].state == possibleWinTileType) { 
          this.winningTiles = new int[] { 2, 4, 6 };
          return true;
        }
      }
      return false;
    }

    bool checkDraw()
    {
      var tiles = gameModel.tiles;
      int tileCount = 9;
      int markedTileCount = 0;

      for (int i = 0; i < tileCount; i++)
      {
        if (tiles[i].state != TileState.CLEAN)
        {
          markedTileCount++;
        }
      }

      return markedTileCount == tileCount;
    }

    public override void Execute() {
      if (checkRows() || checkColumns() || checkDiagonals() || checkDraw())
      {
        gameModel.gameInProgress = false;
        gameEndSignal.Dispatch(this.winningTiles);
      }
    }
  }
}