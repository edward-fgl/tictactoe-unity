using System;
using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using System.Collections.Generic;

namespace tictactoe.game {
  public class CleanupGamefieldCommand : Command {
    [Inject]
    public GameRestartSignal gameRestartSignal { get; set; }

    [Inject]
    public GameModel gameModel { get; set; }

    public override void Execute() {
      for (int i = 0; i < 9; i++) {
        gameModel.tiles[i].state = TileState.CLEAN;
      }

      gameModel.currentPlayer = CurrentPlayer.PLAYER_ONE;
      gameModel.gameInProgress = true;
    }
  }
}