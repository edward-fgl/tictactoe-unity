using System;
using UnityEngine;
using strange.extensions.command.impl;

namespace tictactoe.game {
  public class GameEndCommand : Command {
    [Inject]
    public GameModel gameModel { get; set; }

    public override void Execute() {

    }
  }
}