using System;
using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using System.Collections.Generic;

namespace tictactoe.game {
  public class InitializeGamefieldCommand : Command {
    [Inject]
    public GamefieldInitializedSignal gamefieldInitializedSignal { get; set; }

    [Inject]
    public GameModel gameModel { get; set; }

    public override void Execute() {
      gameModel.tiles = new Dictionary<int, Tile>();
      for (int i = 0; i < 9; i++) {
        gameModel.tiles.Add(i, new Tile(i, TileState.CLEAN));
      }

      gameModel.gameInProgress = true;

      int[] tileIds = new int[gameModel.tiles.Keys.Count];
      gameModel.tiles.Keys.CopyTo(tileIds, 0);
      gamefieldInitializedSignal.Dispatch(tileIds);
    }
  }
}