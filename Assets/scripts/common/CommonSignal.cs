using System;
using strange.extensions.signal.impl;

namespace tictactoe {
  public class StartSignal : Signal{}

  public class GameStartSignal : Signal{}
  public class GameRestartSignal : Signal{}
  public class GameEndSignal : Signal<int[]>{}
}